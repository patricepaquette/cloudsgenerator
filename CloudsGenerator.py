import bpy
from numpy import random


#material parameters
mat_name = 'Colorful_Clouds' #Enter the name of the material you want to apply to clouds

#clouds parameters
numberOfClouds = 10 #Enter how many clouds you want to generate
minCloudSizeX = 6; maxCloudSizeX = 8 #Enter the min and max scale of X for each generated Cloud
minCloudSizeY = 6; maxCloudSizeY = 12 #Enter the min and max scale of Y for each generated Cloud
minCloudSizeZ = 9; maxCloudSizeZ = 12 #Enter the min and max scale of Z for each generated Cloud
minCloudLocX = -150; maxCloudLocX = 150 #Enter the min and max scale of X for each generated Cloud
minCloudLocY = -150; maxCloudLocY = 150 #Enter the min and max scale of Y for each generated Cloud
minCloudLocZ = -150; maxCloudLocZ = 150 #Enter the min and max scale of Z for each generated Cloud

#metaballs parameters
numberOfBallsByCloud = 50 #Enter the number of metaballs to be used for a cloud
minBallsScaleX = 25; maxBallsScaleX = 100 #Enter the min and max scale of X for each generated Ball
minBallsScaleY = 25; maxBallsScaleY = 100 #Enter the min and max scale of Y for each generated Ball
minBallsScaleZ = 25; maxBallsScaleZ = 100 #Enter the min and max scale of Z for each generated Ball
minBallsLocX = -3; maxBallsLocX =  3 #Enter the min and max locations of X for each generated Ball
minBallsLocY = -5; maxBallsLocY =  5 #Enter the min and max locations of Y for each generated Ball
minBallsLocZ = -1; maxBallsLocZ =  2 #Enter the min and max locations of Z for each generated Ball

#setting the number of clouds to create
for i in range(numberOfClouds): 
    #setting the number of metaballs per clouds
    for i in range(numberOfBallsByCloud):
        #setting random location and scale of metaballs 
        bpy.ops.object.metaball_add(type='ELLIPSOID', enter_editmode=False, align='WORLD', location=(random.randint(minBallsLocX,maxBallsLocX), random.randint(minBallsLocY,maxBallsLocY), random.randint(minBallsLocZ,maxBallsLocZ)), scale=(random.randint(minBallsScaleX,maxBallsScaleX), random.randint(minBallsScaleY,maxBallsScaleY), random.randint(minBallsScaleZ,maxBallsScaleZ)))
    
    #converting the metaballs to mesh creating a solid cloud
    bpy.ops.object.convert(target='MESH')
    
    #set origin point to geometry
    bpy.ops.object.origin_set(type='GEOMETRY_ORIGIN', center='MEDIAN')

    #randomly resizing of the cloud
    bpy.ops.transform.resize(value=(random.randint(minCloudSizeX,maxCloudSizeX), random.randint(minCloudSizeY,maxCloudSizeY), random.randint(minCloudSizeZ,maxCloudSizeZ)), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
    
    #apply the scale of the cloud
    bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
    
    #setting random location of the cloud
    bpy.ops.transform.translate(value=(random.randint(minCloudLocX,maxCloudLocX), random.randint(minCloudLocY,maxCloudLocY), random.randint(minCloudLocZ,maxCloudLocZ)), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, release_confirm=True)
    
    #applying the selected material to the cloud
    bpy.context.object.active_material = bpy.data.materials[mat_name]


    #go in Edit Mode
    bpy.ops.object.mode_set(mode = 'EDIT')
    #select the mesh of the cloud
    bpy.ops.mesh.select_all(action= 'SELECT')
    #convert the triangles faces to quads
    bpy.ops.mesh.tris_convert_to_quads()
    #unsubdividing the mesh to reduce number of vertex
    bpy.ops.mesh.unsubdivide(iterations=3)    
    
    #making sure the view is in 3DView so we apply UV from view
    for area in bpy.context.screen.areas:
        if area.type == 'VIEW_3D':
            for region in area.regions:
                if region.type == 'WINDOW':
                    override = {'area': area, 'region': region, 'edit_object': bpy.context.edit_object}
                    #applying UV from view
                    bpy.ops.uv.project_from_view(override , camera_bounds=False, correct_aspect=True, scale_to_bounds=True)
    
    #go back in Object Mode
    bpy.ops.object.mode_set(mode = 'OBJECT')